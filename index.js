//khai báo thư viện express
const express = require('express');

//khai báo thêm một đường dẫn
const path = require("path");

//khỡi tạo app express
const app = express();
const port = 8000;



app.get('/', (req,res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + '/views/task42.10.html'))

})
//thêm ảnh vào middleware static
app.use(express.static(__dirname + '/views'))

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})